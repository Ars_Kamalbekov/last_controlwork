<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class InstitutionAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('isActive')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('avatar', null,
                array('template' => 'AppBundle:Admin:avatar_list.html.twig'))
            ->add('name')
            ->add('description')
            ->add('isActive')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextType::class,[
                'label'=>'Название'
            ])
            ->add('avatarFile', FileType::class,[
                'label'=>'Главная Фотка'
            ] )
            ->add('description', TextType::class,[
                'label'=> 'Описание'
            ])
            ->add('user')
            ->add('isActive', CheckboxType::class,[
                'label' => 'Статус'
            ])
            ->add('category', EntityType::class, [
                'class' => 'AppBundle\Entity\Category',
            ])
            ->add('user', EntityType::class, [
                'class' => 'AppBundle\Entity\User',
            ])

        ;

        if ($this->isCurrentRoute('edit')) {
            $formMapper
                ->add('isActive', CheckboxType::class,[
                    'label' => 'Статус'
                ])
            ;
        }
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('avatar')
            ->add('name')
            ->add('description')
            ->add('isActive')
            ->add('reviews', EntityType::class, [
                'class' => 'AppBundle\Entity\Review',
            ])
        ;
    }
}
