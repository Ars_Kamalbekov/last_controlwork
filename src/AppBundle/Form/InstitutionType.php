<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InstitutionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'label'=>'Название',
                    ])
            ->add('description', TextareaType::class,[
                'label'=>'Описание'
            ])
            ->add('category', EntityType::class, [
                'class' => 'AppBundle\Entity\Category',
                'placeholder' => 'Выберите ',
                'label' => 'Категория'
            ])
            ->add('avatarFile', FileType::class,[
                'label'=>'Главное фото'
            ])
            ->add('submit', SubmitType::class,[
                'label'=>'Добавить новое место'
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_institution_type';
    }
}
