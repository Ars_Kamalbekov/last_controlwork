<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class addReviewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextareaType::class,[
                'label'=>'Добавить отзыв',
                'required'=>false
            ])
            ->add('food_quality',  ChoiceType::class,[
                'choices' =>[
                    '' => null,
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5
                ],
                'expanded' => false,
                'multiple' => false,
                'label'=> 'Качество еды'
            ])
            ->add('service_quality', ChoiceType::class,[
                'choices' =>[
                    '' => null,
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5
                ],
                'expanded' => false,
                'multiple' => false,
                'label'=> 'Качество обслуживания'
            ])
            ->add('interior_quality', ChoiceType::class,[
                'choices' =>[
                    '' => null,
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5
                ],
                'expanded' => false,
                'multiple' => false,
                'label'=> 'Интерьер'
            ])
            ->add('submit', SubmitType::class,[
                'label' => 'Оставить отзыв'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundleadd_review_type';
    }
}
