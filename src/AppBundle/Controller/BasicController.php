<?php

namespace AppBundle\Controller;

use AppBundle\Form\searchInstitutionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class BasicController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $institutions = $this->getDoctrine()
            ->getRepository('AppBundle:Institution')
            ->findAll();

        $categories = $this->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->findAll();

        return $this->render('@App/Basic/index.html.twig', [
            'institutions' => $institutions,
            'categories' => $categories,
        ]);
    }

//    /**
//     * @Route("/{category_name}", name="show_category")
//     * @param string $category_name
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @internal param Request $request
//     */
//    public function showCategoryAction(string $category_name)
//    {
//        $category = $this->getDoctrine()
//            ->getRepository('AppBundle:Category')
//            ->findByName($category_name);
//        $isCategory = false;
//        return $this->render('@App/Basic/index.html.twig', [
//            'isCategory'=>$isCategory,
//            'institutions' => $institutions,
//            'categories' => $categories
//        ]);
//    }


}
