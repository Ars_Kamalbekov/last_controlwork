<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Image;
use AppBundle\Entity\Institution;
use AppBundle\Entity\Review;
use AppBundle\Form\addImageType;
use AppBundle\Form\addReviewType;
use AppBundle\Libs\RatingManager;
use AppBundle\Form\InstitutionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class InstitutionController extends Controller
{
    /**
     * @Method({"GET", "POST"})
     * @Route("/add_place",name="add_place")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addInstitutionAction(Request $request)
    {
        $institution = new Institution();

        $form = $this->createForm(InstitutionType::class, $institution);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $institution->setIsActive(false);
            $institution->setUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($institution);
            $em->flush();

            $session = $this->get('session');
            $session->getFlashBag()->add('greeting', 'Вы успешно добавили свое заведение. 
            Ждите пока Админ одобрит вашу заявку');

            return $this->redirectToRoute('homepage');

        }
        return $this->render('AppBundle:Institution:add_institution.html.twig', array(
            'form' => $form->createView()
        ));
    }


    /**
     * @Route("/institution_{id}"), name="show_institution")
     * @param int $id
     * @param Request $request
     * @param RatingManager $ratingManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showInstitutionAction(int $id, Request $request, RatingManager $ratingManager)
    {
        $user = $this->getUser();
        if (!$user) {
            $this->redirectToRoute('fos_user_security_login');
        }
        $institution = $this->getDoctrine()
            ->getRepository('AppBundle:Institution')
            ->find($id);

        $review_form = $this->createForm(addReviewType::class, null, [
            'action' => $this->generateUrl(
                'add_review', ['institution_id' => $institution->getId()]),
            'method' => 'POST'
        ]);
        $image_form = $this->createForm(addImageType::class, null, [
            'action' => $this->generateUrl(
                'add_image', ['institution_id' => $institution->getId()]),
            'method' => 'POST'
        ]);

        $all_scores = $ratingManager->getAllScores($institution);
        $overral_scores = $ratingManager->getOverralScore();

        return $this->render('AppBundle:Institution:show_institution.html.twig', array(
            'institution' => $institution,
            'image_form' => $image_form->createView(),
            'review_form' => $review_form->createView(),
            'user' => $this->getUser(),
            'all_scores' => $all_scores,
            'overral_scores' => $overral_scores
        ));
    }

    /**
     * @Method("DELETE")
     * @Route("/delete_review/{institution_id}", name="delete_review")
     * @param Request $request
     * @param int $institution_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */
    public function deleteReviewAction(Request $request, int $institution_id)
    {

    }

    /**
     * @Method("POST")
     * @Route("/add_image/{institution_id}", name="add_image")
     * @param Request $request
     * @param int $institution_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */
    public function addImageAction(Request $request, int $institution_id)
    {
        $institution = $this->getDoctrine()
            ->getRepository('AppBundle:Institution')
            ->find($institution_id);

        $image = new Image();
        $image_form = $this->createForm(addImageType::class, $image);

        $image_form->handleRequest($request);


        if ($image_form->isValid()) {
            $image->setUser($this->getUser());
            $image->setInstitution($institution);
            $em = $this->getDoctrine()->getManager();
            $em->persist($image);
            $em->flush();
        }
        return $this->redirectToRoute(
            'app_institution_showinstitution',
            ['id' => $institution_id]);

    }


    /**
     * @Method("POST")
     * @Route("/add_review/{institution_id}", name="add_review")
     * @param Request $request
     * @param int $institution_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */
    public function addReviewAction(Request $request, int $institution_id)
    {

        $review = new Review();

        $institution = $this->getDoctrine()
            ->getRepository('AppBundle:Institution')
            ->find($institution_id);

        $form = $this->createForm(addReviewType::class, $review);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $review_repository = $this->getDoctrine()->getRepository('AppBundle:Review');
            $current_user = $this->getUser();
            $review_repository->saveNewReview($review, $current_user, $institution);
        }
        return $this->redirectToRoute('app_institution_showinstitution', ['id' => $institution_id]);
    }


    /**
     * @Method("GET")
     * @Route("/search_institution", name="search_institution")
     */
    public function getSelectInstitution()
    {
        $requested_word = $_GET['word'];
        $institution_repository = $this->getDoctrine()->getRepository('AppBundle:Institution');
        /** @var Institution $institution */
        $institution = $institution_repository->getSelectedInstitution($requested_word);
        if (empty($institution)) {
            $session = $this->get('session');
            $session->getFlashBag()->add('greeting', 'Извините, 
            но к сожалению у нас нет такого заведения в базе данных');
            return $this->redirectToRoute('homepage');
        }
        /** @var Institution $found_institution */
        $found_institution = $institution[0];
        return $this->redirectToRoute('app_institution_showinstitution', [
            'id' => $found_institution->getId()
        ]);
    }
}
