<?php
/**
 * Created by PhpStorm.
 * User: arsen
 * Date: 15.11.17
 * Time: 19:15
 */

namespace AppBundle\Libs;

use AppBundle\Entity\Institution;
use AppBundle\Entity\Review;
use Doctrine\ORM\EntityManager;

class RatingManager
{
    private $allScores;
    private $entityManager;
    private $overral_scores;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getOverralScore()
    {
        return $this->overral_scores;
    }

    public function getAllScores(Institution $institution): array
    {
        $em = $this->entityManager;
        /** @var Institution $institution */
        $reviews = $institution->getReviews();
        /** @var Review $reviews */
        foreach ($reviews as $review) {
            /** @var Review $review */
            if ($review->getInteriorQuality() != 0) {
                $this->allScores [0] [] = $review->getFoodQuality();
                $this->allScores [1] [] = $review->getServiceQuality();
                $this->allScores [2] [] = $review->getInteriorQuality();
            }
        }
        if(count($this->allScores[0]) > 0){
            $food = round(array_sum($this->allScores[0]) / count($this->allScores[0]), 2);
            $service = round(array_sum($this->allScores[1]) / count($this->allScores[1]), 2);
            $interior = round(array_sum($this->allScores[2]) / count($this->allScores[2]), 2);
            $overral = ($food + $service + $interior) / 3;
            $this->overral_scores = round($overral, 2);
            $institution->setOverralScore($this->getOverralScore());
            $em->flush();

            return [
                'food' => $food,
                'service' => $service,
                'interior' => $interior,
            ];
        }

        return [
            'food' => 0,
            'service' => 0,
            'interior' => 0,
        ];
    }

}