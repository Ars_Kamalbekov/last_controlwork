<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Review
 *
 * @ORM\Table(name="review")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReviewRepository")
 */
class Review
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text",nullable=true)
     */
    private $description;

    /**
     * @var int
     * @ORM\Column(name="food_quality", type="integer", nullable=false)
     */
    private $food_quality ;

    /**
     * @var int
     * @ORM\Column(name="service_quality", type="integer", nullable=false)
     */
    private $service_quality;

    /**
     * @var int
     * @ORM\Column(name="interior_quality", type="integer", nullable=false)
     */
    private $interior_quality ;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="reviews")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Institution", inversedBy="reviews")
     */
    private $institution;

    /**
     * @var \DateTime
     * @ORM\Column(name="publish_date", type="datetime", nullable=true)
     */
    private $publishDate;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    public function __toString()
    {
        return $this->description ?: '';
    }
    /**
     * Set user
     *
     * @param string $user
     *
     * @return Review
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set institution
     *
     * @param \AppBundle\Entity\Institution $institution
     *
     * @return Review
     */
    public function setInstitution(\AppBundle\Entity\Institution $institution = null)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Get institution
     *
     * @return \AppBundle\Entity\Institution
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Review
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set foodQuality
     *
     * @param integer $foodQuality
     *
     * @return Review
     */
    public function setFoodQuality($foodQuality)
    {
        $this->food_quality = $foodQuality;

        return $this;
    }

    /**
     * Get foodQuality
     *
     * @return integer
     */
    public function getFoodQuality()
    {
        return $this->food_quality;
    }

    /**
     * Set serviceQuality
     *
     * @param integer $serviceQuality
     *
     * @return Review
     */
    public function setServiceQuality($serviceQuality)
    {
        $this->service_quality = $serviceQuality;

        return $this;
    }

    /**
     * Get serviceQuality
     *
     * @return integer
     */
    public function getServiceQuality()
    {
        return $this->service_quality;
    }

    /**
     * Set interiorQuality
     *
     * @param integer $interiorQuality
     *
     * @return Review
     */
    public function setInteriorQuality($interiorQuality)
    {
        $this->interior_quality = $interiorQuality;

        return $this;
    }

    /**
     * Get interiorQuality
     *
     * @return integer
     */
    public function getInteriorQuality()
    {
        return $this->interior_quality;
    }

    /**
     * Set publishDate
     *
     * @param \DateTime $publishDate
     *
     * @return Review
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;

        return $this;
    }

    /**
     * Get publishDate
     *
     * @return \DateTime
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }
}
