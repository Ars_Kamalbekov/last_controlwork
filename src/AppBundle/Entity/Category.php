<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Institution", mappedBy="category" )
     */
    private $institutions;

    public function __construct()
    {
        $this->institutions = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name ?: '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    
    /**
     * Add institution
     *
     * @param \AppBundle\Entity\Institution $institution
     *
     * @return Category
     */
    public function addInstitution(\AppBundle\Entity\Institution $institution)
    {
        $this->institutions[] = $institution;

        return $this;
    }

    /**
     * Remove institution
     *
     * @param \AppBundle\Entity\Institution $institution
     */
    public function removeInstitution(\AppBundle\Entity\Institution $institution)
    {
        $this->institutions->removeElement($institution);
    }

    /**
     * Get institutions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInstitutions()
    {
        return $this->institutions;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
