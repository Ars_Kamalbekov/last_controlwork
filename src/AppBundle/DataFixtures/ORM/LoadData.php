<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use AppBundle\Entity\Institution;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin
            ->setEmail('original@some.com')
            ->setUsername('black.admin')
            ->setRoles(['ROLE_ADMIN'])
            ->setEnabled(true);

        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($admin, 'admin');
        $admin->setPassword($password);
        $manager->persist($admin);

        for($i=0; $i < 3; $i++){
            $user = new User();
            $user->setUsername('user'.$i);
            $user->setEmail('user$'.$i.'.com');
            $user->setIsActive(true);
            $user->setEnabled(true);

            $encoder = $this->container->get('security.password_encoder');
            $password = $encoder->encodePassword($admin, 'user'.$i);
            $user->setPassword($password);
            $manager->persist($user);
        }

        $categories = ['Ресторан', 'Кафе', 'Паб', 'Таверна', 'Фаст-Фуд', 'Бар'];
        foreach ($categories as $category_name){
            $category = new Category();
            $category->setName($category_name);
            $manager->persist($category);
        }

        $manager->flush();
    }
}